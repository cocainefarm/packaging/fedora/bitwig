Name:           bitwig-studio
Version:        3.1.2
Release:        1%{?dist}
Summary:        A DAW

License:        Custom
URL:            https://www.bitwig.com
Source0:        https://downloads.bitwig.com/stable/%{version}/bitwig-studio-%{version}.deb

BuildRequires:  binutils

%description


%prep
ar x bitwig-studio-%{version}.deb
mkdir -p bitwig-studio
tar -C ./bitwig-studio -xvf data.tar.xz

%install
mkdir -p %{buildroot}/opt/bitwig-studio
cd ./bitwig-studio
find opt -type d -print0 | 
while IFS= read -r -d '' file; do
    install -d --mode 755 "$file" "%{buildroot}/$file"
done

find opt -type f -print0 | 
while IFS= read -r -d '' file; do
    install -D --mode 644 "$file" "%{buildroot}/$file"
done

%files
/opt/bitwig-studio
%license /opt/bitwig-studio/EULA.txt
%docdir /opt/bitwig-studio/resources/doc


%changelog
* Thu Dec 26 2019 Max Audron <audron@cocaine.farm>
- 
